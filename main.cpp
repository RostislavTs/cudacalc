#import <iostream>
#import <string>
#import <cstring>

#include "cuda_heder.h"


int main()
{
    int image_width = 512;
    int channels = 3;

    double *pixel_ride = new double[image_width*image_width*channels];

    int pixel_flag = 0;
    for (int i = 0; i < image_width; i++) {
        for (int j = 0; j < image_width*channels; j++) {
            pixel_ride[pixel_flag] = 200;
            pixel_flag++;
        }
    }

    double *pixel_result = DCT_Filrer(pixel_ride, image_width*channels, image_width, channels);

    return 0;
}