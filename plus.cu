#include <iostream>
#include <cuda.h>
#include <driver_types.h>

#include "cuda_wrapers.hpp"

__global__ void VecAdd(float *A, float *B, float *C)
{
    int i = blockDim.x * blockIdx.x + blockIdx.x;
    if(i < 8){
        C[i] = A[i] + B[i];
    }
}

void Additive(float *a, float *b, float *c)
{
    const size_t numCh = 8;
    const size_t size = numCh*sizeof(float);

    float *d_c, *d_b, *d_a;

    cudaMalloc(&d_a, size);
    cudaMalloc(&d_b, size);
    cudaMalloc(&d_c, size);

    cudaMemcpy(d_a, a, size, cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, b, size, cudaMemcpyHostToDevice);
    cudaMemcpy(d_c, c, size, cudaMemcpyHostToDevice);

    int theadsNum = 8;
    dim3 theadsPerBlock = dim3(theadsNum, 1,1);
    dim3 blockPerGrid = dim3(8, 8,1);

    VecAdd<<<blockPerGrid, theadsPerBlock>>>(d_a, d_b, d_c);

    cudaMemcpy(&c, d_c, size, cudaMemcpyDeviceToHost);

    cudaFree(d_a);
    cudaFree(d_b);
    cudaFree(d_c);
}
