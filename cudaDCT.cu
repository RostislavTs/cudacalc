//
// Created by rostislav on 08.02.19.
//

#include <cuda.h>
#include <driver_types.h>
#include <iostream>

const int width_ = 512;
const int height_ = 512;
const int dct_mtx_size_ = 8;

__global__ void matrix_multiply(int *image, int matrix_delay, float *dct_matrix, int width, int height, float *dctmtx)
{
    int blockIdi = blockIdx.x * 64;
    float dev_block_now[64];
    float dev_block_now_transp[64];

    for(int t = 0; t < matrix_delay; t++)
    {
        int i_block = 0;
        for (int j = (blockIdi + (t*64)); j < (blockIdi + ((t+1)*64)); j++, i_block++) {
            int s = int(i_block / 8);
            for (int i = 0; i < 8; i++) {
                dev_block_now_transp[i_block] += dctmtx[(s * 8)+i] * image[j+(i*8)+s];
            }
            dct_matrix[j] = dev_block_now_transp[i_block];
        }
        i_block = 0;
        for (int j = (blockIdi + (t*64)); j < (blockIdi + ((t+1)*64)); j++, i_block++) {
            int s = int(i_block / 8);
            int k = i_block % 8;
            for (int i = 0; i < 8; i++) {
                dev_block_now[i_block] += dev_block_now_transp[i + (s*8)] * dctmtx[(k*8) + i];
            }
            dct_matrix[j] = dev_block_now[i_block];
        }
        for(int i = 0; i < 64; i++) {
            dev_block_now[i] = 0.0;
            dev_block_now_transp[i] = 0.0;
        }
    }
}

__global__ void mean(float *dct_matrix, int length, float *dct)
{
    int blockIdi = blockIdx.x;
    float summ = 0.0;
    int count = 0;
    for(int i = 0; i < length; i++)
    {
        summ += dct_matrix[blockIdi + (i*64)];
        count++;
    }
    dct[blockIdi] = summ/count;
}

int main()
{
    int image[width_*height_];
    float dct[64];
    for(int i = 0; i < width_; i++)
    {
        for (int j = 0; j < height_; j++) {
            image[i + (j*511)] = 100;
        }
    }
    int size_image_DCT_block = (width_/dct_mtx_size_)*(height_/dct_mtx_size_);
    float dct_matrix[size_image_DCT_block];
    float dctmtx[64] = {0.3536, 0.3536, 0.3536, 0.3536, 0.3536, 0.3536, 0.3536, 0.3536, 0.4904, 0.4157, 0.2778, 0.0975, -0.0975, -0.2778, -0.4157, -0.4904, 0.4619, 0.1913, -0.1913, -0.4619, -0.4619, -0.1913, 0.1913, 0.4619, 0.4157, -0.0975, -0.4904, -0.2778, 0.2778, 0.4904, 0.0975, -0.4157, 0.3536, -0.3536, -0.3536, 0.3536, 0.3536, -0.3536, -0.3536, 0.3536, 0.2778,   -0.4904,    0.0975,    0.4157,   -0.4157,   -0.0975,    0.4904,   -0.2778, 0.1913, -0.4619,    0.4619,  -0.1913,  -0.1913,    0.4619,   -0.4619,    0.1913, 0.0975,   -0.2778,    0.4157,   -0.4904,    0.4904,   -0.4157,    0.2778,   -0.0975};

    int N = width_*height_; // size image

    int *dev_image; //  Dev array for image
    float *dev_matrix_dct; // Dev array for dct coff
    float *dev_dctmtx;
    float *dev_dct;
    cudaMalloc((void**)&dev_image, sizeof(int)*N);
    cudaMemcpy(dev_image, image, N*sizeof(int), cudaMemcpyHostToDevice);
    cudaMalloc((void**)&dev_dctmtx, sizeof(float)*64);
    cudaMalloc((void**)&dev_dct, sizeof(float)*64);
    cudaMemcpy(dev_dctmtx, dctmtx, 64*sizeof(float), cudaMemcpyHostToDevice);
    cudaMalloc((void**)&dev_matrix_dct, sizeof(float)*N*size_image_DCT_block); // Malloc for array dct

    //int delay = (int)(size_image_DCT_block/1024) / 2;
    //int *dev_delay;
    //int *dev_width;
    //int *dev_height;
    //cudaMalloc(&dev_delay, sizeof(int));
    //cudaMemcpy(dev_delay, delay, sizeof(int), cudaMemcpyHostToDevice);

    matrix_multiply<<<1024, 1>>>(dev_image, 4, dev_matrix_dct, 512, 512, dev_dctmtx);
    mean<<<64, 1>>>(dev_dctmtx, 1024, dev_dct);
    cudaMemcpy(&dct, dev_dct, sizeof(float)*64, cudaMemcpyDeviceToHost);

    std::cout << dct[0];

    cudaFree(dev_matrix_dct);

    return 0;
}
